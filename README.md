# teatimershell

Small tea timer shell script. Written after yet another bag of black tea was left in the cup for too long.

Queries the user for a type of tea, then sets a timer accordingly. Audibly notifies the user when ready. Can optionally display notifcations.

# Supported teas:
1) black [4m30s]
2) white/green [2m30s]
3) herbal [8m]
4) fruit [9m]
5) mate (short) [3m]
6) mate (long) [5m]

# Supported languages:
1) English

# Requires: 
1) espeak
2) zenity (optional)