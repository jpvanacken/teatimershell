#! /bin/bash
## teatimershell
# Small tea timer shell script. Written after yet another bag of black tea was left in the cup for too long.
# Queries the user for a type of tea, then sets a timer accordingly. Audibly notifies the user when ready. Can optionally display notifcations.
#
# author: @jpvanacken
# license: GPL-3.0
# version: 2022-06-01

## Requirement checks:
# 
command -v espeak >/dev/null 2>&1 || { echo >&2 "I require espeak but it's not installed.  Aborting."; exit 1; }


## Constants
# If speak_intro is set to TRUE the program will use espeak to play a greeting.
# If use_zenity is set to TRUE the program will use zenity on top of espeak to notify the user.
# If use_zenity is set to TRUE and use_popup is set to TRUE then zenity will display a notification.
# If use_zenity is set to TRUE and use_popup is set to FALSE then zenity will display an info dialog.
speak_intro=TRUE
use_zenity=TRUE
use_popup=FALSE

## Variables
pick=99; byebye=FALSE;

## Sanity-check for zenity:
#
if [ $use_zenity = TRUE ] ; then 
	command -v zenity  >/dev/null 2>&1 || { echo >&2 "Unable to find zenity. Disabling zenity features. To get rid of this warning either install zenity or edit this file accordingly."; echo >&2 "" ; use_zenity=FALSE; }
fi

##
# Start up:
##
echo "Please state the nature of the tea emergency."
echo " "
if [ $speak_intro = TRUE ] ; then espeak "Please state the nature of the tea emergency."  2>/dev/null ; fi
echo "1: Black tea (e.g. Earl Grey) [4m30]"
echo "2: White or green tea (incl. Peppermint) [2m30]"
echo "3: Herbal tea (e.g. Rooibos) [8m]"
echo "4: Fruit based tea [9m]"
echo "5: Mate (short) [3m]"
echo "6: Mate (long) [5m]"
echo "0: Nothing, thanks."
echo " "

##
# Selection loop:
##

while [ $pick -eq 99 ]; do
	echo "Your choice:" 
	read pick
	echo ""
	if [ $pick -eq 1 ]; then
		echo "You chose black tea. Timer set to 4m30s. Please wait."
		espeak "bläc tea, please wait" 2>/dev/null
		sleep 270
		espeak "Your bläc" 2>/dev/null
	elif [ $pick -eq 2 ]; then
		echo "You chose white/green tea. Timer set to 2m30s. Please wait."
		espeak "white, or, green tea, please wait" 2>/dev/null
		sleep 150
		espeak "Your" 2>/dev/null
	elif [ $pick -eq 3 ]; then
		echo "You chose herbal tea. Timer set to 8m. Please wait."
		espeak "herbal tea, please wait" 2>/dev/null
		sleep 480
		espeak "Your herbal" 2>/dev/null
	elif [ $pick -eq 4 ]; then
		echo "You chose fruit based tea. Timer set to 9m. Please wait."
		espeak "fruit based tea, please wait" 2>/dev/null
		sleep 540
		espeak "Your fruit based" 2>/dev/null
	elif [ $pick -eq 5 ]; then
		echo "You chose mate tea. Timer set to 3m. Please wait."
		espeak "Mahteh tea, please wait" 2>/dev/null
		sleep 180
		espeak "Your mahteh" 2>/dev/null
	elif [ $pick -eq 6 ]; then
		echo "You chose mate tea. Timer set to 5m. Please wait."
		espeak "Mahteh tea, please wait" 2>/dev/null
		sleep 300
		espeak "Your mahteh" 2>/dev/null
	elif [ $pick -eq 0 ]; then
		byebye=TRUE
	else
		echo "Input not recognized. Please select one of the following:"
		echo ""
		echo "1: Black tea"
		echo "2: White or green tea"
		echo "3: Herbal tea"
		echo "4: Fruit based tea"
		echo "5: Mate (short)"
		echo "6: Mate (long)"
		echo "0: Nothing, thanks."
		echo ""
		pick=99
	fi
done

##
# Closing interaction.
##

if [ $byebye = TRUE ] ; then
	echo "Bye bye!"
	espeak "Bye bye" 2>/dev/null
else
	echo "Your tea is ready, enjoy!"
	espeak "tea is ready, enjoy" 2>/dev/null
	if [ $use_zenity = TRUE ] ; then
		if [ $use_popup = TRUE ] ; then
			zenity --notification --text="Your tea is ready, enjoy!"
		else
			zenity --info --title="Tea timer shell" --text="Your tea is ready, enjoy!"
		fi
	fi
fi
